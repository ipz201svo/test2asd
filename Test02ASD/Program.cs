using System;
using System.Collections.Generic;

namespace Test02ASD
{
    class Program
    {
        private static void Main(string[] args)
        {
            var array = new[] { 4, 1, 55, 8, 2, 17, 4, 35, 5 };
            Console.WriteLine("Input");
            foreach (var i in array)
            {
                Console.WriteLine(i);
            }

            RadixSort(array);
            Console.WriteLine();
            Console.WriteLine("Output");
            foreach (var i in array)
            {
                Console.WriteLine(i);
            }

        }

        private static void RadixSort(int[] arr)
        {
            // Знайдемо найбільше число для того щоб дізнатись кількість розрядів
            var max = GetMax(arr);

            for (var exp = 1; max / exp > 0; exp *= 10)
                CountSort(arr, exp);
        }

        // Функція, щоб робити сортування підрахунком відповідно до розряду
        private static void CountSort(int[] arr, int exp)
        {
            var output = new int[arr.Length]; // вихідний масив
            int i;
            var count = new int[10];

            count.Initialize();

            // Підрахунок випадків
            for (i = 0; i < arr.Length; i++)
                count[arr[i] / exp % 10]++;

            for (i = 1; i < 10; i++)
                count[i] += count[i - 1];

            // Побудова вихідного масиву
            for (i = arr.Length - 1; i >= 0; i--)
            {
                output[count[arr[i] / exp % 10] - 1] = arr[i];
                count[arr[i] / exp % 10]--;
            }

            // Копіювання вихідного масиву в arr[].
            // Тепер він містить відсортований масив відповідно до розряду
            
            Array.Copy(output, arr, arr.Length);
        }

        
        private static int GetMax(int[] arr)
        {
            var mx = arr[0];
            for (var i = 1; i < arr.Length; i++)
                if (arr[i] > mx)
                    mx = arr[i];
            return mx;
        }
    }
}
